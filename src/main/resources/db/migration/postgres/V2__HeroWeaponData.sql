INSERT INTO weapon (weapon_id, weapon_name) VALUES (1, 'knife');
INSERT INTO weapon (weapon_id, weapon_name) VALUES (2, 'spoon');
INSERT INTO weapon (weapon_id, weapon_name) VALUES (3, 'fork');

INSERT INTO hero (hero_id, hero_name) VALUES (1, 'tick');
INSERT INTO hero (hero_id, hero_name) VALUES (2, 'trick');
INSERT INTO hero (hero_id, hero_name) VALUES (3, 'track');

INSERT INTO hero_weapon (hero_id, weapon_id) VALUES (1, 1);
INSERT INTO hero_weapon (hero_id, weapon_id) VALUES (2, 2);
INSERT INTO hero_weapon (hero_id, weapon_id) VALUES (3, 3);
INSERT INTO hero_weapon (hero_id, weapon_id) VALUES (2, 3);
