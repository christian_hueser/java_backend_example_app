package christian.hueser.hero.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import christian.hueser.hero.entity.HeroEntity;

@Repository
@Transactional
public interface IHeroRepository extends JpaRepository<HeroEntity, Long> {

	public List<HeroEntity> findAll();
	
	public HeroEntity getOne(Long id);
		
}