package christian.hueser.hero.controller;

import org.springframework.web.bind.annotation.RestController;

import christian.hueser.hero.entity.HeroEntity;
import christian.hueser.hero.service.IHeroService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
public class HeroController {

	@Autowired
	private IHeroService heroService;
	
	@GetMapping(path = "/heroes", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<HeroEntity>> getAllHeroes() {
		List<HeroEntity> list = heroService.getAllHeroes();
		return new ResponseEntity<List<HeroEntity>>(list, HttpStatus.OK);
	}

}