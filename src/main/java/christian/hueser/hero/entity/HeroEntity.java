package christian.hueser.hero.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity(name = "Hero")
@Table(name = "hero")
public class HeroEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="hero_id")
	private Long heroId;
	
	@Column(name="hero_name")
	private String heroName;
	
	@ManyToMany
	@JoinTable(
	   name="hero_weapon",
	   joinColumns=@JoinColumn(name="hero_id"),
	   inverseJoinColumns=@JoinColumn(name="weapon_id"))
	private Set<WeaponEntity> weapons  = new HashSet<WeaponEntity>();
	
	public HeroEntity() {

	}
	
	public HeroEntity(Long heroId, String heroName) {
		this.heroId = heroId;
		this.heroName = heroName;
	}
	
	public Long getHeroId() {
		return this.heroId;
	}
	
	public void setHeroId(Long heroId) {
		this.heroId = heroId;
	}
	
	public String getHeroName() {
		return this.heroName;
	}
	
	public void setHeroName(String heroName) {
		this.heroName = heroName;
	}
	
	public Set<WeaponEntity> getWeapons() {
		return this.weapons;
	}
	
	public void setWeapons(Set<WeaponEntity> weapons) {
		this.weapons = weapons;
	}
	
	
	
}
