package christian.hueser.hero.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import christian.hueser.hero.entity.HeroEntity;

@Service
@Transactional
public interface IHeroService {

	public List<HeroEntity> getAllHeroes();
	
    public HeroEntity getHeroByHeroId(Long heroId);
	
}
